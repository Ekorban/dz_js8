/*Теоретичні питання
1. Опишіть своїми словами що таке Document Object Model (DOM)
Document Object Model (DOM) – це інтерфейс, за допомогою якого програми можуть працювати з контентом, стилями
веб-сторінки та її структурою.

2.Яка різниця між властивостями HTML-елементів innerHTML та innerText?
Властивість innerText повертає тільки текст, без пробілів та тегів внутрішніх елементів.
Властивість innerHTML повертає текст, включно зі всіма інтервалами та теги внутрішніх елементів.

3.Як можна звернутися до елемента сторінки за допомогою JS? Який спосіб кращий?
До елементів верхнього рівня можна звернутися безпосередньо, використовуючи властивості об'єкта document:
-document.documentElement - для звернення до елементу;
-document.head - для звернення до елементу;
-document.body - для звернення до елементу.

- Об'єкт document має вбудований метод getElementById(), який виконує пошук елемента за його унікальним ідентифікатором id.
- Для пошуку елемента по тегу вікористовується метод getElementsByTagName (), який повертає список всіх елементів,
що задовольняють запиту.
- Для пошуку по глобальному атрибуту class використовується метод getElementsByClassName (), який повертає список
 всіх елементів із зазначеним класом.
- Для будь-якого елементу сторінки доступний метод querySelectorAll (), який повертає список всіх елементів,
що задовольняють заданій CSS-селектору. При його виклику пошук ведеться всередині даного елемента.
- Для будь-якого елементу сторінки доступний метод querySelector (), який повертає перший елемент, що задовольняє
заданому CSS-селектору. При його виклику пошук ведеться всередині даного елемента.

 Метод querySelectorAll() - найзручніший(універсальний), оскільки дозволяє отримати доступ до будь-якого елементу за CSS-селектором.
 */


const elem = document.querySelectorAll('p');
console.log("elem");

document.querySelectorAll("p").forEach(elem => {
    elem.style.color = "#ff0000";
});

const elemWithId = document.getElementById("optionsList");
console.log(elemWithId);

const elemParent = document.getElementById("optionsList").parentElement;
console.log(elemParent);

const elemChildren = document.querySelector("#optionsList").childNodes;
console.log(elemChildren);

const elemChange = document.querySelector("#testParagraph");
console.log(elemChange.innerHTML);
elemChange.innerHTML = "This is a paragraph";


const elemChange2 = document.querySelector(".main\-header");
console.log(elemChange2.className);
elemChange2.className = "nav\-item";

const elemSectionTitle = document.querySelector(".section\-title");
console.log(elemSectionTitle.className);
elemSectionTitle.classList.remove ("section-title");






